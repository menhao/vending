package org.lsmr.vending.selection;

import org.lsmr.vending.ProductKind;

/**
 * Permits objects to listen to one or more {@link SelectionFacade}s when
 * registered with them.
 * 
 * @author Robert J. Walker
 */
public interface SelectionFacadeListener {
    /**
     * Signals an event in which a selection has occurred.
     * 
     * @param communicationFacade
     *            The facade in which the event occurred.
     * @param productKind
     *            The kind of the selected item. Can be null if the facade has
     *            not been configured correctly.
     */
    public void selected(SelectionFacade communicationFacade, ProductKind productKind);
}
