package org.lsmr.vending.selection;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.lsmr.vending.ProductKind;
import org.lsmr.vending.hardware.AbstractHardwareDevice;
import org.lsmr.vending.hardware.AbstractHardwareDeviceListener;
import org.lsmr.vending.hardware.AbstractHardwareFacade;
import org.lsmr.vending.hardware.SelectionDevice;
import org.lsmr.vending.hardware.SelectionDeviceListener;

/**
 * Contains all functionality dealing with users' selections.
 * 
 * @author Robert J. Walker
 */
public class SelectionFacade {
    private final Set<SelectionFacadeListener> listeners = new HashSet<>();
    private final Map<Object, ProductKind> bindings = new HashMap<>();

    private class SBL implements SelectionDeviceListener {
	@Override
	public void enabled(AbstractHardwareDevice<? extends AbstractHardwareDeviceListener> hardware) {
	    // ignore
	}

	@Override
	public void disabled(AbstractHardwareDevice<? extends AbstractHardwareDeviceListener> hardware) {
	    // ignore
	}

	@Override
	public void selected(SelectionDevice device, Object selection) {
	    notifySelected(bindings.get(selection));
	}
    }

    /**
     * Basic constructor.
     * 
     * @param hf
     *            The hardware facade to be used to implement low-level
     *            functionality.
     */
    public SelectionFacade(AbstractHardwareFacade hf) {
	if(hf == null)
	    throw new IllegalArgumentException();

	SBL sbl = new SBL();
	Iterator<SelectionDevice> iterator = hf.selectionDeviceIterator();
	while(iterator.hasNext())
	    iterator.next().register(sbl);
    }

    /**
     * Permits bindings between codes and {@link ProductKind} s to
     * be specified.
     * 
     * @param bindings
     *            A map of the bindings from code objects to {@link ProductKind}
     *            s. More than one code can map to a given {@link ProductKind}.
     */
    public void configure(Map<Object, ProductKind> bindings) {
	if(bindings == null)
	    throw new IllegalArgumentException();
	this.bindings.putAll(bindings);
    }

    /**
     * Registers the given listener so that it will receive events from this
     * communication facade.
     * 
     * @param listener
     *            The listener to be registered. If it is already registered,
     *            this call has no effect.
     */
    public void register(SelectionFacadeListener listener) {
	listeners.add(listener);
    }

    private void notifySelected(ProductKind pk) {
	for(SelectionFacadeListener listener : listeners)
	    listener.selected(this, pk);
    }
}
