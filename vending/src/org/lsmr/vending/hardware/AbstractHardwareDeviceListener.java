package org.lsmr.vending.hardware;

/**
 * This class represents the abstract interface for all
 * org.lsmr.vending.hardware listeners. All subclasses should add
 * their own event notification methods, the first parameter of which should
 * always be the instance affected.
 */
public interface AbstractHardwareDeviceListener {
    /**
     * Announces that the indicated org.lsmr.vending.hardware has been
     * enabled.
     * 
     * @param hardware
     *            The device that has been enabled.
     */
    public void enabled(AbstractHardwareDevice<? extends AbstractHardwareDeviceListener> hardware);

    /**
     * Announces that the indicated org.lsmr.vending.hardware has been
     * disabled.
     * 
     * @param hardware
     *            The device that has been enabled.
     */
    public void disabled(AbstractHardwareDevice<? extends AbstractHardwareDeviceListener> hardware);
}
