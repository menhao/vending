package org.lsmr.vending.hardware;

/**
 * Abstract base class for devices that support selections.
 * 
 * @author Robert J. Walker
 */
public abstract class SelectionDevice extends AbstractHardwareDevice<SelectionDeviceListener> {
    /**
     * Causes a selection to be on the device that is equivalent to the
     * indicated code.
     * 
     * @param code
     *            The code indicating the selection. Subclasses can define
     *            constraints on legal codes.
     */
    public abstract void makeSelection(Object code);
}
