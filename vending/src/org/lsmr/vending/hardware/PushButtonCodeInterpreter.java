package org.lsmr.vending.hardware;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Listens to push button presses to determine a two-character code.
 * 
 * @author Robert J. Walker
 */
public class PushButtonCodeInterpreter extends SelectionDevice implements SelectionDeviceListener {
    private final PushButton[] buttons;
    private final Map<PushButton, Character> characters;

    private enum State {
			START,
			WAIT
    }

    private State state = State.START;
    private StringBuilder sb = new StringBuilder();

    /**
     * Basic constructor.
     * 
     * @param characters
     *            A list of the characters to be represented. A
     *            {@link PushButton} will be added for each and mapped to the
     *            corresponding character.
     */
    public PushButtonCodeInterpreter(List<Character> characters) {
	if(characters == null || characters.isEmpty())
	    throw new IllegalArgumentException();

	this.buttons = new PushButton[characters.size()];
	for(int i = 0, max = characters.size(); i < max; i++) {
	    this.buttons[i] = new PushButton();
	    this.buttons[i].register(this);
	}

	int index = 0;
	this.characters = new HashMap<>();
	for(Character c : characters)
	    this.characters.put(this.buttons[index++], c);
    }

    /**
     * If the state is START or WAIT, appends a valid code to the string being
     * built; moves from START to WAIT or from WAIT to START in these
     * circumstances. A bad button press resets the state to START, eliminating
     * any current code.
     */
    @Override
    public void selected(SelectionDevice device, Object selection) {
	PushButton button = (PushButton)device;
	Character c = characters.get(button);
	if(state == State.START) {
	    if(c != null && Character.isLetter(c)) {
		sb = new StringBuilder();
		sb.append(c);
		state = State.WAIT;
	    }
	    else
		sb = new StringBuilder();
	}
	else {
	    if(c != null && Character.isDigit(c)) {
		sb.append(c);
		state = State.START;
		notifyCodeEntered();
	    }
	    else {
		state = State.START;
		sb = new StringBuilder();
	    }
	}
    }

    /**
     * Returns the current code as a string. If the code is incomplete, the
     * string may be empty or truncated.
     * 
     * @return The current code.
     */
    public String read() {
	return sb.toString();
    }

    private void notifyCodeEntered() {
	for(SelectionDeviceListener l : listeners)
	    l.selected(this, read());
    }

    @Override
    public void enabled(AbstractHardwareDevice<? extends AbstractHardwareDeviceListener> hardware) {
    }

    @Override
    public void disabled(AbstractHardwareDevice<? extends AbstractHardwareDeviceListener> hardware) {
    }

    /**
     * Accesses a selection button at the indicated index.
     * 
     * @param index
     *            The index of the desired selection button.
     * @return The relevant device.
     * @throws IndexOutOfBoundsException
     *             if the index &lt; 0 or the index &gt;= number of selection
     *             selectionButtons.
     */
    public PushButton getPushButton(int index) {
	return buttons[index];
    }

    /**
     * Determines the index of the given button within this hardware facade.
     * 
     * @param button
     *            The button to find.
     * @return The index of the indicated button, or -1 if it does not exist
     *         within this hardware facade.
     */
    public int indexOf(PushButton button) {
	for(int i = 0; i < buttons.length; i++)
	    if(buttons[i] == button)
		return i;
	return -1;
    }

    /**
     * The code is expected to be a 2-character String object.
     */
    @Override
    public void makeSelection(Object code) {
	String str = (String)code;
	if(str.length() != 2)
	    throw new IllegalArgumentException("The input code must contain two characters");

	char firstChar = str.charAt(0), secondChar = str.charAt(1);
	PushButton first = null, second = null;

	for(PushButton button : characters.keySet()) {
	    Character c = characters.get(button);

	    if(c == firstChar)
		first = button;
	    else if(c == secondChar)
		second = button;
	}

	if(first == null || second == null)
	    throw new IllegalArgumentException("The two characters must be in the ranges A-F and 0-9");

	first.press();
	second.press();
    }
}
