package org.lsmr.vending.hardware;

/**
 * Represents a simple push button on the vending machine. It ignores the
 * enabled/disabled state.
 */
public final class PushButton extends SelectionDevice {
    /**
     * Simulates the pressing of the button. Notifies its listeners of a
     * "pressed" event.
     */
    public void press() {
	notifyPressed();
    }

    /**
     * The code is expected to be identical to this button.
     */
    @Override
    public void makeSelection(Object code) {
	if(code != this)
	    throw new IllegalArgumentException();
	press();
    }

    private void notifyPressed() {
	for(SelectionDeviceListener listener : listeners)
	    listener.selected(this, this);
    }
}
