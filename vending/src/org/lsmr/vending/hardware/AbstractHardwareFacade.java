package org.lsmr.vending.hardware;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.lsmr.vending.Cents;

/**
 * An abstract representation of hardware facades.
 * 
 * @author Robert J. Walker
 */
public abstract class AbstractHardwareFacade {
    private boolean safetyOn = false;
    private Set<HardwareFacadeListener> listeners = new HashSet<>();
    private Lock lock;
    private Cents[] coinKinds;
    private CoinSlot coinSlot;
    private CoinReceptacle receptacle;
    private StorageBin storageBin;
    private DeliveryChute deliveryChute;
    private CoinRack[] coinRacks;
    private Map<Cents, CoinChannel> coinRackChannels;
    private ProductRack[] productRacks;
    private Display display;
    private PushButton returnButton;
    private IndicatorLight exactChangeLight;
    private IndicatorLight outOfOrderLight;

    /**
     * Creates a standard arrangement for the hardware. All the components are
     * created and interconnected. The hardware is initially empty. The product
     * kind names and costs are initialized to &quot; &quot; and 1 respectively.
     * 
     * 
     * @param coinKinds
     *            The values (in cents) of each kind of coin. The order of the
     *            kinds is maintained. One coin rack is produced for each kind.
     *            Each kind must have a unique, positive value.
     * @param productRackCount
     *            The number of product racks in the machine. Must be
     *            positive.
     * @param coinRackCapacity
     *            The maximum capacity of each coin rack in the machine. Must be
     *            positive.
     * @param productRackCapacity
     *            The maximum capacity of each product rack in the machine. Must
     *            be positive.
     * @param receptacleCapacity
     *            The maximum capacity of the coin receptacle, storage bin, and
     *            delivery chute. Must be positive.
     * @throws IllegalArgumentException
     *             If any of the arguments is null, or the size of productCosts
     *             and productNames differ.
     */
    protected AbstractHardwareFacade(Cents[] coinKinds, int productRackCount, int coinRackCapacity, int productRackCapacity, int receptacleCapacity) {
	if(coinKinds == null)
	    throw new IllegalArgumentException("Arguments may not be null");

	if(productRackCount < 1 || coinRackCapacity < 1 || productRackCapacity < 1)
	    throw new IllegalArgumentException("Counts and capacities must be positive");

	if(coinKinds.length < 1)
	    throw new IllegalArgumentException("At least one coin kind must be accepted");

	this.coinKinds = Arrays.copyOf(coinKinds, coinKinds.length);

	Set<Cents> currentCoinKinds = new HashSet<>();
	for(Cents coinKind : coinKinds) {
	    Cents zero = new Cents(0);
	    if(coinKind.compareTo(zero) < 1)
		throw new IllegalArgumentException("Coin kinds must have positive values");

	    if(currentCoinKinds.contains(coinKind))
		throw new IllegalArgumentException("Coin kinds must have unique values");

	    currentCoinKinds.add(coinKind);
	}

	lock = new Lock();
	display = new Display();
	coinSlot = new CoinSlot(coinKinds);
	receptacle = new CoinReceptacle(receptacleCapacity);
	storageBin = new StorageBin();
	
	deliveryChute = new DeliveryChute(receptacleCapacity);
	coinRacks = new CoinRack[coinKinds.length];
	coinRackChannels = new HashMap<Cents, CoinChannel>();
	for(int i = 0; i < coinKinds.length; i++) {
	    coinRacks[i] = new CoinRack(coinRackCapacity);
	    coinRacks[i].connect(new CoinChannel(deliveryChute));
	    coinRackChannels.put(coinKinds[i], new CoinChannel(coinRacks[i]));
	}

	productRacks = new ProductRack[productRackCount];
	for(int i = 0; i < productRackCount; i++) {
	    productRacks[i] = new ProductRack(i, productRackCapacity);
	    productRacks[i].connect(new ProductChannel(deliveryChute));
	}

	returnButton = new PushButton();

	coinSlot.connect(new CoinChannel(receptacle), new CoinChannel(deliveryChute));
	receptacle.connect(coinRackChannels, new CoinChannel(deliveryChute), new CoinChannel(storageBin));

	exactChangeLight = new IndicatorLight();
	outOfOrderLight = new IndicatorLight();
    }

    /**
     * Accessor for the lock device.
     * 
     * @return The lock device.
     */
    public Lock getLock() {
	return lock;
    }

    /**
     * Accessor for the coin kind at the indicated index.
     * 
     * @param index
     *            The desired index. Must be in [0, max).
     * @return The coin kind at the desired index.
     */
    public Cents getCoinKind(int index) {
	return coinKinds[index];
    }

    /**
     * Disables all the components of the hardware that involve physical
     * movements. Activates the out of order light.
     */

    public void enableSafety() {
	safetyOn = true;
	coinSlot.disable();
	deliveryChute.disable();

	for(int i = 0; i < productRacks.length; i++)
	    productRacks[i].disable();

	for(int i = 0; i < coinRacks.length; i++)
	    coinRacks[i].disable();

	outOfOrderLight.activate();
    }

    /**
     * Enables all the components of the hardware that involve physical
     * movements. Deactivates the out of order light.
     */

    public void disableSafety() {
	safetyOn = false;
	coinSlot.enable();
	deliveryChute.enable();

	for(int i = 0; i < productRacks.length; i++)
	    productRacks[i].enable();

	for(int i = 0; i < coinRacks.length; i++)
	    coinRacks[i].enable();

	outOfOrderLight.deactivate();
    }

    /**
     * Returns whether the safety is currently engaged.
     * 
     * @return true if the safety is enabled; false if the safety is disabled.
     */

    public boolean isSafetyEnabled() {
	return safetyOn;
    }

    /**
     * Accesses the exact change light.
     * 
     * @return The relevant device.
     */

    public IndicatorLight getExactChangeLight() {
	return exactChangeLight;
    }

    /**
     * Accesses the out of order light.
     * 
     * @return The relevant device.
     */

    public IndicatorLight getOutOfOrderLight() {
	return outOfOrderLight;
    }

    /**
     * Accessor for an iterator over the selection devices directly supported by
     * this device.
     * 
     * @return The iterator.
     */
    public abstract Iterator<SelectionDevice> selectionDeviceIterator();

    /**
     * Accesses the money return button.
     * 
     * @return The money return button.
     */

    public PushButton getReturnButton() {
	return returnButton;
    }

    /**
     * Accesses the coin slot.
     * 
     * @return The relevant device.
     */
    public CoinSlot getCoinSlot() {
	return coinSlot;
    }

    /**
     * Accesses the coin receptacle.
     * 
     * @return The relevant device.
     */
    public CoinReceptacle getCoinReceptacle() {
	return receptacle;
    }

    /**
     * Accesses the storage bin.
     * 
     * @return The relevant device.
     */
    public StorageBin getStorageBin() {
	return storageBin;
    }

    /**
     * Accesses the delivery chute.
     * 
     * @return The relevant device.
     */
    public DeliveryChute getDeliveryChute() {
	return deliveryChute;
    }

    /**
     * Accesses the count of coin racks in this machine.
     * 
     * @return The count.
     */
    public int getNumberOfCoinRacks() {
	return coinRacks.length;
    }

    /**
     * Accesses the coin rack at the indicated index.
     * 
     * @param index
     *            The desired index. Must be in [0, max).
     * @return The relevant device.
     */
    public CoinRack getCoinRack(int index) {
	return coinRacks[index];
    }

    /**
     * Accesses the coin rack for the indicated coin kind.
     * 
     * @param kind
     *            The desired coin kind.
     * @return The relevant coin rack, or null if none such exists.
     */
    public CoinRack getCoinRackForCoinKind(Cents kind) {
	CoinChannel cc = coinRackChannels.get(kind);
	if(cc != null){
		CoinRack rack = (CoinRack)cc.getSink();
	    return rack;
	}
	return null;
    }

    /**
     * Accesses the coin kind indicated for the coin rack at the indicated
     * index.
     * 
     * @param index
     *            The desired index. Must be in [0, max).
     * @return The relevant coin kind.
     */
    public Cents getCoinKindForCoinRack(int index) {
	return coinKinds[index];
    }

    /**
     * Accesses the count of product racks
     * 
     * @return The count.
     */
    public int getNumberOfProductRacks() {
	return productRacks.length;
    }

    /**
     * Accesses the product rack at the indicated index.
     * 
     * @param index
     *            The desired index. Must be in [0, max).
     * @return The relevant device.
     */
    public ProductRack getProductRack(int index) {
	return productRacks[index];
    }

    /**
     * Creates an iterator over the product racks, in order of increasing index.
     * 
     * @return The iterator.
     */
    public Iterator<ProductRack> productRackIterator() {
	return new Iterator<ProductRack>() {
	    private int index = 0;

	    @Override
	    public boolean hasNext() {
		return index < getNumberOfProductRacks();
	    }

	    @Override
	    public ProductRack next() {
		return getProductRack(index++);
	    }
	};
    }

    /**
     * Accesses the textual display.
     * 
     * @return The relevant device.
     */
    public Display getDisplay() {
	return display;
    }

    /**
     * Registers the given listener with this facade so that the listener will
     * be notified of events emanating from here.
     * 
     * @param listener
     *            The listener to be registered. No effect if it is already
     *            registered. Cannot be null.
     */
    public void register(HardwareFacadeListener listener) {
	listeners.add(listener);
    }

    /**
     * De-registers the given listener from this facade so that the listener
     * will no longer be notified of events emanating from here.
     * 
     * @param listener
     *            The listener to be de-registered. No effect if it is not
     *            already registered or is null.
     */
    public void deregister(HardwareFacadeListener listener) {
	listeners.remove(listener);
    }

    /**
     * Accesses the count of devices that can be used for selection. This should
     * not include nested devices.
     * 
     * @return The count.
     */
    public abstract int getNumberOfSelectionDevices();

    /**
     * Accesses the selection device at the indicated index.
     * 
     * @param index
     *            The desired index. Must be in [0, max).
     * @return The relevant device.
     */
    public abstract SelectionDevice getSelectionDevice(int index);
}
