package org.lsmr.vending.hardware;

/**
 * Listens for events emanating from a selection button.
 * 
 * @author Robert J. Walker
 */
public interface SelectionDeviceListener extends AbstractHardwareDeviceListener {
    /**
     * An event that is announced to the listener when a selection has occurred
     * on the indicated device.
     * 
     * @param device
     *            The device on which the event occurred.
     * @param selection
     *            The item that has been selected. Concrete classes will define
     *            what this means.
     */
    public void selected(SelectionDevice device, Object selection);
}
