package org.lsmr.vending.hardware;

import java.util.Iterator;

import org.lsmr.vending.Cents;

/**
 * Represents a standard configuration of the vending machine hardware:
 * <ul>
 * <li>one coin slot;</li>
 * <li>one coin receptacle (called the coin receptacle) to temporarily store
 * coins entered by the user;</li>
 * <li>one coin receptacle (called the storage bin) to store coins that have
 * been accepted as payment;</li>
 * <li>a set of one or more coin racks (the number and the denomination of coins
 * stored by each is specified in the constructor);</li>
 * <li>one delivery chute used to deliver products and to return coins;</li>
 * <li>a set of one or more product racks (the number, cost, and product name
 * stored in each is specified in the constructor);</li>
 * <li>one textual display;</li>
 * <li>one money return button;</li>
 * <li>a set of one or more selection selectionButtons (exactly one per product
 * rack); and</li>
 * <li>two indicator lights: one to indicate that exact change should be used by
 * the user; the other to indicate that the machine is out of order.</li>
 * </ul>
 * <p>
 * The component devices are interconnected as follows:
 * <ul>
 * <li>the output of the coin slot is connected to the input of the coin
 * receptacle;</li>
 * <li>the outputs of the coin receptacle are connected to the inputs of the
 * coin racks (for valid coins to be stored therein), the delivery chute (for
 * invalid coins or other coins to be returned), and the storage bin (for coins
 * to be accepted that do not fit in the coin racks);</li>
 * <li>the output of each coin rack is connected to the delivery chute; and</li>
 * <li>the output of each product rack is connected to the delivery chute.</li>
 * </ul>
 * <p>
 * Each component device can be disabled to prevent any physical movements.
 * Other functionality is not affected by disabling a device; hence devices that
 * do not involve physical movements are not affected by "disabling" them.
 * <p>
 * Most component devices have some sort of maximum capacity (e.g., of the
 * number of products that can be stored therein). In some cases, this is a
 * simplification of the physical reality for the sake of simulation.
 */
public final class PopHardwareFacade extends AbstractHardwareFacade {
    private PushButton[] selectionButtons;

    /**
     * Creates a standard arrangement for the hardware. All the components are
     * created and interconnected. The hardware is initially empty. The product
     * kind names and costs are initialized to &quot; &quot; and 1 respectively.
     * 
     * 
     * @param coinKinds
     *            The values (in cents) of each kind of coin. The order of the
     *            kinds is maintained. One coin rack is produced for each kind.
     *            Each kind must have a unique, positive value.
     * @param productRackCount
     *            The number of product racks in the machine. Must be
     *            positive.
     * @param coinRackCapacity
     *            The maximum capacity of each coin rack in the machine. Must be
     *            positive.
     * @param productRackCapacity
     *            The maximum capacity of each product rack in the machine. Must
     *            be positive.
     * @param receptacleCapacity
     *            The maximum capacity of the coin receptacle, storage bin, and
     *            delivery chute. Must be positive.
     * @throws IllegalArgumentException
     *             If any of the arguments is null, or the size of productCosts
     *             and productNames differ.
     */
    public PopHardwareFacade(Cents[] coinKinds, int productRackCount, int coinRackCapacity, int productRackCapacity, int receptacleCapacity) {
	super(coinKinds, productRackCount, coinRackCapacity, productRackCapacity, receptacleCapacity);

	if(productRackCount < 1 || coinRackCapacity < 1 || productRackCapacity < 1)
	    throw new IllegalArgumentException("Counts and capacities must be positive");

	selectionButtons = new PushButton[productRackCount];
	for(int i = 0; i < productRackCount; i++)
	    selectionButtons[i] = new PushButton();
    }

    /**
     * Accesses a selection button at the indicated index.
     * 
     * @param index
     *            The index of the desired selection button.
     * @return The relevant device.
     * @throws IndexOutOfBoundsException
     *             if the index &lt; 0 or the index &gt;= number of selection
     *             selectionButtons.
     */
    public PushButton getPushButton(int index) {
	return selectionButtons[index];
    }

    /**
     * Determines the index of the given button within this hardware facade.
     * 
     * @param button
     *            The button to find.
     * @return The index of the indicated button, or -1 if it does not exist
     *         within this hardware facade.
     */
    public int indexOf(PushButton button) {
	for(int i = 0; i < selectionButtons.length; i++)
	    if(selectionButtons[i] == button)
		return i;
	return -1;
    }

    @Override
    public Iterator<SelectionDevice> selectionDeviceIterator() {
	return new Iterator<SelectionDevice>() {
	    private int index = 0;

	    @Override
	    public boolean hasNext() {
		return index < getNumberOfSelectionDevices();
	    }

	    @Override
	    public SelectionDevice next() {
		return getPushButton(index++);
	    }
	};
    }

    @Override
    public int getNumberOfSelectionDevices() {
	return selectionButtons.length;
    }

    @Override
    public SelectionDevice getSelectionDevice(int index) {
	return getPushButton(index);
    }
}
