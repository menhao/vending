package org.lsmr.vending.hardware;

import org.lsmr.vending.ProductKind;

/**
 * Listens for events emanating from the hardware facade. Most events emanate
 * from individual pieces of hardware, so those individual pieces have to be
 * listened to in order to be notified of such events.
 * 
 * @author Robert J. Walker
 */
public interface HardwareFacadeListener extends AbstractHardwareDeviceListener {
    /**
     * An event that is announced when the hardware is configured.
     * 
     * @param hf
     *            The facade that has been configured
     * @param kinds
     *            The product kinds that have been set.
     */
    public void configured(PopHardwareFacade hf, ProductKind... kinds);
}
