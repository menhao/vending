package org.lsmr.vending.hardware;

import java.util.ArrayList;
import java.util.List;

import org.lsmr.vending.Coin;

public class StorageBin implements AbstractCoinAcceptor{

	ArrayList<Coin> coins = new ArrayList<>();
	@Override
	public void acceptCoin(Coin coin) throws CapacityExceededException, DisabledException {
		coins.add(coin);
	}

	@Override
	public boolean hasSpace() {
		return true;
	}

	public List<Coin> unload() {
		List<Coin> result = new ArrayList<>(coins);
		coins.clear();
		return result;
	}
}
