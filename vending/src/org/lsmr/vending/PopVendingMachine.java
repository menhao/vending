package org.lsmr.vending;

import java.util.List;
import java.util.Map;

import org.lsmr.vending.funds.FundsFacade;
import org.lsmr.vending.hardware.CoinRack;
import org.lsmr.vending.hardware.AbstractHardwareFacade;
import org.lsmr.vending.hardware.PopHardwareFacade;
import org.lsmr.vending.hardware.SimulationException;
import org.lsmr.vending.product.ProductFacade;
import org.lsmr.vending.rules.SelectionRule;
import org.lsmr.vending.selection.SelectionFacade;

/**
 * Represents pop vending machines, fully configured and with all software
 * installed.
 * 
 * @author Robert J. Walker
 */
public class PopVendingMachine implements IVendingMachine {
    private PopHardwareFacade hf;
    private SelectionFacade sf;
    private FundsFacade ff;
    private ProductFacade pf;

    @Override
    public AbstractHardwareFacade getHardware() {
	return hf;
    }

    /**
     * Creates a standard arrangement for the vending machine. All the
     * components are created and interconnected. The hardware is initially
     * empty. The product kind names and costs are initialized to &quot; &quot;
     * and 1 respectively.
     * 
     * @param coinKinds
     *            The values (in cents) of each kind of coin. The order of the
     *            kinds is maintained. One coin rack is produced for each kind.
     *            Each kind must have a unique, positive value.
     * @param selectionButtonCount
     *            The number of selection buttons on the machine. Must be
     *            positive.
     * @param coinRackCapacity
     *            The maximum capacity of each coin rack in the machine. Must be
     *            positive.
     * @param productRackCapacity
     *            The maximum capacity of each product rack in the machine. Must
     *            be positive.
     * @param receptacleCapacity
     *            The maximum capacity of the coin receptacle, storage bin, and
     *            delivery chute. Must be positive.
     * @throws IllegalArgumentException
     *             If any of the arguments is null, or the size of productCosts
     *             and productNames differ.
     */
    PopVendingMachine(Cents[] coinKinds, int selectionButtonCount, int coinRackCapacity, int productRackCapacity, int receptacleCapacity) {
	hf = new PopHardwareFacade(coinKinds, selectionButtonCount, coinRackCapacity, productRackCapacity, receptacleCapacity);
	ff = new FundsFacade(hf);
	sf = new SelectionFacade(hf);
	pf = new ProductFacade(hf);
	new SelectionRule(sf, ff, pf);
    }

    @Override
    public void configure(Map<Object, ProductKind> kindBindings, Map<ProductKind, List<Integer>> rackBindings) {
	if(kindBindings.values().size() != rackBindings.keySet().size())
	    throw new IllegalArgumentException();
	
	sf.configure(kindBindings);
	pf.configure(rackBindings);
    }

    @Override
    public void loadCoins(int... coinCounts) {
	if(coinCounts.length != hf.getNumberOfCoinRacks())
	    throw new SimulationException("Coin counts have to equal number of racks");

	int i = 0;
	for(int coinCount : coinCounts) {
	    if(coinCount < 0)
		throw new SimulationException("Each count must not be negative");

	    CoinRack cr = hf.getCoinRack(i);
	    Cents value = hf.getCoinKindForCoinRack(i);
	    for(int coins = 0; coins < coinCount; coins++)
		cr.load(new Coin(value));

	    i++;
	}
    }

    @Override
    public void loadProducts(ProductKind kind, int amount) {
    	String name = kind.getName();
    	Product[] products = new Product[amount];
    	for(int i=0;i<amount;i++){
    		products[i] = new Product(name);
    	}
    	
    	pf.load(kind, products);
    }

	@Override
	public ProductFacade getProductFacade() {
		return pf;
	}
}
