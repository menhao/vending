package org.lsmr.vending;

import java.util.List;
import java.util.Map;

import org.lsmr.vending.hardware.AbstractHardwareFacade;
import org.lsmr.vending.hardware.SimulationException;
import org.lsmr.vending.product.ProductFacade;

/**
 * An abstract representation of vending machines.
 * 
 * @author Robert J. Walker
 */
public interface IVendingMachine {
    /**
     * Accessor for the hardware facade for this vending machine.
     * 
     * @return The hardware.
     */
    public AbstractHardwareFacade getHardware();

    public ProductFacade getProductFacade();
    /**
     * Permits bindings between codes and {@link ProductKind} s to be specified
     * as well as from.
     * 
     * @param kindBindings
     *            A map of the bindings from code objects to {@link ProductKind}
     *            s. More than one code can map to a given {@link ProductKind}.
     * @param rackBindings
     *            A map of the bindings from {@link ProductKind}s to the list of
     *            integers representing the indices of corresponding product
     *            racks.
     */
    public void configure(Map<Object, ProductKind> kindBindings, Map<ProductKind, List<Integer>> rackBindings);

    /**
     * A convenience method for constructing and loading a set of coins into the
     * machine.
     * 
     * @param coinCounts
     *            A variadic list of ints each representing the number of coins
     *            to create and load into the corresponding rack.
     * @throws SimulationException
     *             If the number of arguments is different than the number of
     *             racks, or if any of the counts are negative.
     */
    public void loadCoins(int... coinCounts);

    /**
     * A convenience method for constructing and loading a set of coins into the
     * machine.
     * 
     * @param productCounts
     *            A variadic list of ints each representing the number of
     *            products to create and load into the corresponding rack.
     * @throws SimulationException
     *             If the number of arguments is different than the number of
     *             racks, or if any of the counts are negative.
     */
    public void loadProducts(ProductKind kind, int amount);
}
