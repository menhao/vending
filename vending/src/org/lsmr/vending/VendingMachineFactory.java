package org.lsmr.vending;

/**
 * Permits vending machines of a particular, configured kind to be constructed.
 * The configuration can change as desired.
 * 
 * @author Robert J. Walker
 */
public abstract class VendingMachineFactory {
    private static VendingMachineFactory instance;

    static {
	configure(Configuration.POP);
    }

    private static class PopVendingMachineFactory extends VendingMachineFactory {
	public IVendingMachine newVendingMachine(Cents[] coinKinds, int productRackCount, int coinRackCapacity, int productRackCapacity,
		int receptacleCapacity) {
	    return new PopVendingMachine(coinKinds, productRackCount, coinRackCapacity, productRackCapacity, receptacleCapacity);
	}
    }

    private static class CandyVendingMachineFactory extends VendingMachineFactory {
	public IVendingMachine newVendingMachine(Cents[] coinKinds, int productRackCount, int coinRackCapacity, int productRackCapacity,
		int receptacleCapacity) {
	    return new CandyVendingMachine(coinKinds, productRackCount, coinRackCapacity, productRackCapacity, receptacleCapacity);
	}
    }

    /**
     * Accesses the currently configured factory.
     * 
     * @return The factory.
     */
    public static VendingMachineFactory getFactory() {
	return instance;
    }

    /**
     * Permits the factory to be configured to construct a particular kind of
     * vending machine.
     * 
     * @param c
     *            The configuration.
     */
    public static void configure(Configuration c) {
	switch(c) {
	case CANDY:
	    instance = new CandyVendingMachineFactory();
	    break;
	default:
	    instance = new PopVendingMachineFactory();
	}
    }

    /**
     * Creates a standard arrangement for the vending machine. All the
     * components are created and interconnected. The hardware is initially
     * empty. The product kind names and costs are initialized to &quot; &quot;
     * and 1 respectively.
     * 
     * @param coinKinds
     *            The values (in cents) of each kind of coin. The order of the
     *            kinds is maintained. One coin rack is produced for each kind.
     *            Each kind must have a unique, positive value.
     * @param productRackCount
     *            The number of selection buttons on the machine. Must be
     *            positive.
     * @param coinRackCapacity
     *            The maximum capacity of each coin rack in the machine. Must be
     *            positive.
     * @param productRackCapacity
     *            The maximum capacity of each product rack in the machine. Must
     *            be positive.
     * @param receptacleCapacity
     *            The maximum capacity of the coin receptacle, storage bin, and
     *            delivery chute. Must be positive.
     * @throws IllegalArgumentException
     *             If any of the arguments is null, or the size of productCosts
     *             and productNames differ.
     * @return The new instance.
     */
    public abstract IVendingMachine newVendingMachine(Cents[] coinKinds, int productRackCount, int coinRackCapacity, int productRackCapacity,
	    int receptacleCapacity);
}
