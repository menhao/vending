package org.lsmr.vending.rules;

import org.lsmr.vending.Cents;
import org.lsmr.vending.ProductKind;
import org.lsmr.vending.funds.FundsFacade;
import org.lsmr.vending.hardware.CapacityExceededException;
import org.lsmr.vending.hardware.DisabledException;
import org.lsmr.vending.hardware.EmptyException;
import org.lsmr.vending.hardware.ProductRack;
import org.lsmr.vending.hardware.SimulationException;
import org.lsmr.vending.product.ProductFacade;
import org.lsmr.vending.selection.SelectionFacade;
import org.lsmr.vending.selection.SelectionFacadeListener;

/**
 * This rule deals with &quot;selected&quot; events from the communication
 * facade, coordinating product dispensing, funds entry, funds return, etc.
 * 
 * @author Robert J. Walker
 */
public class SelectionRule {
    private final FundsFacade ff;
    private final ProductFacade pf;

    private class CL implements SelectionFacadeListener {
	@Override
	public void selected(SelectionFacade sf, ProductKind pk) {
	    Cents cost = pk.getCost();
	    boolean enoughFund = ff.hasAvailableFunds(cost);
	    ProductRack rack = pf.getNonEmptyRackForProduct(pk);
	    
	    try {
		    if(enoughFund && rack!=null) {
			    ff.storeFunds(cost);
			    rack.dispenseProduct();
			    audit(rack);
			}
		    
		    ff.returnUnusedFunds();
	    }
	    catch(EmptyException | DisabledException | CapacityExceededException e) {
		    throw new SimulationException(e);
		}
	}
    }

    private void audit(ProductRack rack){
    	int index = rack.getIndex();
    	ProductKind kind = pf.getProductKindForRack(index);
    	String productName = kind.getName();
    	String log = String.format("Audit: Sold %s at rack %d", productName, index);
    	System.out.println(log);
    }
    /**
     * Basic constructor.
     * 
     * @param sf
     *            The communication facade that will be used to listen for
     *            &quot;selected&quot; events indicating that the user wants a
     *            given product.
     * @param ff
     *            The funds facade that will be used to determine the quantity
     *            of funds entered and to return excess.
     * @param pf
     *            The product facade that will be used to determine product
     *            availability and to dispense product.
     */
    public SelectionRule(SelectionFacade sf, FundsFacade ff, ProductFacade pf) {
	if(sf == null || ff == null || pf == null)
	    throw new IllegalArgumentException("The arguments cannot be null");

	this.ff = ff;
	this.pf = pf;

	sf.register(new CL());
    }
}
