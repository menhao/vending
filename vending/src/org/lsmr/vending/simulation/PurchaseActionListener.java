package org.lsmr.vending.simulation;

import java.util.List;

import org.lsmr.vending.Cents;
import org.lsmr.vending.Coin;
import org.lsmr.vending.IVendingMachine;
import org.lsmr.vending.hardware.AbstractHardwareFacade;
import org.lsmr.vending.hardware.CoinSlot;
import org.lsmr.vending.hardware.SelectionDevice;

public class PurchaseActionListener{

	IVendingMachine machine;
	public PurchaseActionListener(IVendingMachine machine){
		this.machine = machine;
	}
	
	public void actionPerformed(Object code){
		AbstractHardwareFacade hardware = machine.getHardware();
		CoinSlot slot = hardware.getCoinSlot();
		slot.addCoin(new Coin(new Cents(100)));
		slot.addCoin(new Coin(new Cents(100)));
		slot.addCoin(new Coin(new Cents(100)));
		SelectionDevice selectionDevice = hardware.getSelectionDevice(0);
		selectionDevice.makeSelection(code);
		List<Object> returnedObjs = Utilities.extractAndSortFromDeliveryChute(hardware);
		int change = (Integer)returnedObjs.get(0);
		String info = String.format("Recieve change %d cents", change);
		if(returnedObjs.size()==2){
			String productName = (String)returnedObjs.get(1);
			info = String.format("%s and product %s", info, productName);
		}
		System.out.println(info);
	}

}
