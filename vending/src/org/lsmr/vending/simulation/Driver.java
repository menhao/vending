package org.lsmr.vending.simulation;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.lsmr.vending.Cents;
import org.lsmr.vending.Configuration;
import org.lsmr.vending.IVendingMachine;
import org.lsmr.vending.ProductKind;
import org.lsmr.vending.VendingMachineFactory;

public class Driver {
	private static IVendingMachine machine;
	private static Object[] codes;
	private static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args){
		String arg = args[0];
		init(arg);
		processCommand();
    	
	}
	
	private static IVendingMachine init(String type){
		Configuration config = Configuration.valueOf(type);
		VendingMachineFactory.configure(config);
		VendingMachineFactory factory = VendingMachineFactory.getFactory();
		
		//valid coin kinds
		Cents[] validCents = new Cents[] {new Cents(5), new Cents(10), new Cents(25), new Cents(100)};
		//valid product kinds
		ProductKind coke = new ProductKind("Coke", new Cents(250));
		ProductKind water = new ProductKind("water", new Cents(250));
		ProductKind stuff = new ProductKind("stuff", new Cents(205));
		
		//create machine
		machine = factory.newVendingMachine(validCents, 3, 10, 10, 10);
		machine.loadCoins(0, 1, 2, 1); //160 cents = 0*5 + 1*10 + 2*25 + 1*100
    	
		//establish mapping from codes to product kinds
		codes = new Object[3];
		if(config == Configuration.POP) {
		    codes[0] = machine.getHardware().getSelectionDevice(0);
		    codes[1] = machine.getHardware().getSelectionDevice(1);
		    codes[2] = machine.getHardware().getSelectionDevice(2);
		}
		else {
		    codes[0] = "A1";
		    codes[1] = "A2";
		    codes[2] = "B1";
		}
		Map<Object, ProductKind> code2kindBindings = new HashMap<>();
		code2kindBindings.put(codes[0], coke);
		code2kindBindings.put(codes[1], water);
		code2kindBindings.put(codes[2], stuff);
		
		//establish mapping from product kinds to racks
		Map<ProductKind, List<Integer>> rackBindings = new HashMap<>();
		rackBindings.put(coke, Arrays.asList(0));
		rackBindings.put(water, Arrays.asList(1));
		rackBindings.put(stuff, Arrays.asList(2));
		
		machine.configure(code2kindBindings, rackBindings);
		machine.loadProducts(coke,1);
		machine.loadProducts(water,1);
		machine.loadProducts(stuff,1);
		return machine;
	}
	
	private static void processCommand(){
		//the following code simulate a typical interactive system
		PurchaseActionListener pListener = new PurchaseActionListener(machine);
		ExitActionListener eListener = new ExitActionListener(machine);
		    	
		boolean exit = false;
	    while(!exit){
			int input = readUserChoice();
			switch(input){
				case 1:
					pListener.actionPerformed(codes[0]);
					System.out.println();
					break;
				default:
					eListener.actionPerformed();
					System.out.println("Exit");
					exit = true;
			}
		}
	}
	
	private static int readUserChoice(){
		System.out.println("Menu");
		System.out.println("0:exit");
		System.out.println("1:purchase");
		System.out.print("Please make a choice:");
		int choice = scanner.nextInt();
		return choice;
	}
}
