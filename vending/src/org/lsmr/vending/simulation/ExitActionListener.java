package org.lsmr.vending.simulation;

import java.util.List;

import org.lsmr.vending.IVendingMachine;
import org.lsmr.vending.hardware.AbstractHardwareFacade;
import org.lsmr.vending.product.ProductFacade;

public class ExitActionListener{

	IVendingMachine machine;
	public ExitActionListener(IVendingMachine machine){
		this.machine = machine;
	}
	
	public void actionPerformed() {
		AbstractHardwareFacade hardware = machine.getHardware();
		int moneyFromRack = Utilities.extractAndSumFromCoinRacks(hardware);
		int moneyFromBin = Utilities.extractAndSumFromStorageBin(hardware);
		int total = moneyFromRack + moneyFromBin;
		String info = String.format("Total %d cents stored in machine", total);
		System.out.println(info);
		
		ProductFacade pf = machine.getProductFacade();
		List<String> products = Utilities.extractAndSortFromProductRacks(pf);
		System.out.print("Product remained in machine:");
		System.out.println(products);
	}

}
