package org.lsmr.vending.simulation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.lsmr.vending.Coin;
import org.lsmr.vending.Product;
import org.lsmr.vending.hardware.AbstractHardwareFacade;
import org.lsmr.vending.hardware.CoinRack;
import org.lsmr.vending.hardware.StorageBin;
import org.lsmr.vending.product.ProductFacade;

/**
 * A set of simple utility methods for use by test cases.
 * 
 * @author Robert J. Walker
 */
public class Utilities {
    /**
     * Extracts all items in the delivery chute of the indicated vending
     * machine, sorting them.
     * 
     * @param hf
     *            The vending machine from which to extract.
     * @return A list of the extracted items.
     */
    public static List<Object> extractAndSortFromDeliveryChute(AbstractHardwareFacade hf) {
	Object[] actualItems = hf.getDeliveryChute().removeItems();
	int actualValue = 0;
	List<Object> actualList = new ArrayList<>();

	for(Object obj : actualItems) {
	    if(obj instanceof Product) {
		Product product = (Product)obj;
		String name = product.getName();
		actualList.add(name);
	    }
	    else
		actualValue += ((Coin)obj).getValue().getValue();
	}

	Collections.sort(actualList, null);

	actualList.add(0, actualValue);
	return actualList;
    }

    /**
     * All coins in the coin racks of the indicated vending machine are
     * extracted.
     *
     * @param hf
     *            The vending machine to extract from.
     * @return The sum of the values of the extracted coins.
     */
    public static int extractAndSumFromCoinRacks(AbstractHardwareFacade hf) {
	int total = 0;

	for(int i = 0, max = hf.getNumberOfCoinRacks(); i < max; i++) {
	    CoinRack cr = hf.getCoinRack(i);
	    List<Coin> coins = cr.unload();
	    for(Coin coin : coins)
		total += coin.getValue().getValue();
	}

	return total;
    }

    /**
     * All coins in the storage bin of the indicated vending machine are
     * extracted.
     *
     * @param hf
     *            The vending machine to extract from.
     * @return The sum of the values of the extracted coins.
     */
    public static int extractAndSumFromStorageBin(AbstractHardwareFacade hf) {
	int total = 0;
	StorageBin bin = hf.getStorageBin();
	List<Coin> coins = bin.unload();
	for(Coin coin : coins)
	    total += coin.getValue().getValue();

	return total;
    }

    /**
     * Extracts all products in the product racks of the indicated vending
     * machine, sorting them.
     * 
     * @param hf
     *            The vending machine from which to extract.
     * @return A list of the extracted items.
     */
    public static List<String> extractAndSortFromProductRacks(ProductFacade pf) {
	List<Product> actualProducts = pf.unload();
	List<String> actualList = new ArrayList<>();
	for(Product product : actualProducts)
	    actualList.add(product.getName());

	Collections.sort(actualList, null);

	return actualList;
    }
}
