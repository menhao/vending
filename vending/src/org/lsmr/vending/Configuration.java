package org.lsmr.vending;

/**
 * Permits the kind of vending machines to be configured.
 * 
 * @author Robert J. Walker
 */
public enum Configuration {
      @SuppressWarnings("javadoc") POP,
      @SuppressWarnings("javadoc") CANDY
}