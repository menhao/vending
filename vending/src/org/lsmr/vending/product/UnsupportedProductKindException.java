package org.lsmr.vending.product;

import org.lsmr.vending.ProductKind;

/**
 * Used to announce when a {@link ProductKind} is accessed that is not supported
 * by the vending machine.
 * 
 * @author Robert J. Walker
 */
public class UnsupportedProductKindException extends RuntimeException {
    private static final long serialVersionUID = -2091596763922888066L;
}
