package org.lsmr.vending.product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.lsmr.vending.Product;
import org.lsmr.vending.ProductKind;
import org.lsmr.vending.hardware.AbstractHardwareDevice;
import org.lsmr.vending.hardware.AbstractHardwareDeviceListener;
import org.lsmr.vending.hardware.CapacityExceededException;
import org.lsmr.vending.hardware.AbstractHardwareFacade;
import org.lsmr.vending.hardware.ProductRack;
import org.lsmr.vending.hardware.ProductRackListener;

/**
 * Controls all interaction with all products.
 * 
 * @author Robert J. Walker
 */
public class ProductFacade {
    private final AbstractHardwareFacade hf;
    private final Set<ProductFacadeListener> listeners = new HashSet<>();
    private Map<Integer, ProductKind> rackToKindMap;
    private ProductKind[] availableKinds;
    
    private class PRL implements ProductRackListener {
	@Override
	public void enabled(AbstractHardwareDevice<? extends AbstractHardwareDeviceListener> hardware) {
	}

	@Override
	public void disabled(AbstractHardwareDevice<? extends AbstractHardwareDeviceListener> hardware) {
	}

	@Override
	public void productAdded(ProductRack productRack, Product product) {
	}

	@Override
	public void productRemoved(ProductRack productRack, Product product) {
	}

	@Override
	public void productsFull(ProductRack productRack) {
	}

	@Override
	public void productsEmpty(ProductRack productRack) {
	}

	@Override
	public void productsLoaded(ProductRack rack, Product... products) {
	}

	@Override
	public void productsUnloaded(ProductRack rack, Product... products) {
	}
    }

    /**
     * Basic constructor.
     * 
     * @param hf
     *            The hardware atop which this facade is to operate. Cannot be
     *            null.
     */
    public ProductFacade(AbstractHardwareFacade hf) {
	if(hf == null)
	    throw new IllegalArgumentException();

	this.hf = hf;
	PRL listener = new PRL();
	for(int i = 0, count = hf.getNumberOfProductRacks(); i < count; i++)
	    hf.getProductRack(i).register(listener);
    }

    /**
     * Defines the set of {@link ProductKind}s supported by this machine and
     * which {@link ProductRack} indices each one maps to.
     * 
     * @param configuration
     *            The above mapping. Cannot be null. Each integer must be in the
     *            range [0, max).
     */
    public void configure(Map<ProductKind, List<Integer>> configuration) {
	if(configuration == null)
	    throw new IllegalArgumentException();

	for(List<Integer> list : configuration.values())
	    for(Integer index : list)
		if(index < 0 || index >= hf.getNumberOfProductRacks())
		    throw new IllegalArgumentException("Each index must be in the range [0, max).");

	int size = configuration.size();
	rackToKindMap = new HashMap<>();
	availableKinds = new ProductKind[size];
	
	int i = 0;
	for(ProductKind pk : configuration.keySet()) {
		availableKinds[i] = pk;
		i++;
	    for(Integer index : configuration.get(pk))
		rackToKindMap.put(index, pk);
	}
    }

    /**
     * Registers the given listener with this facade so that the listener will
     * be notified of events emanating from here.
     * 
     * @param listener
     *            The listener to be registered. No effect if it is already
     *            registered. Cannot be null.
     */
    public void register(ProductFacadeListener listener) {
	listeners.add(listener);
    }

    /**
     * De-registers the given listener from this facade so that the listener
     * will no longer be notified of events emanating from here.
     * 
     * @param listener
     *            The listener to be de-registered. No effect if it is not
     *            already registered or is null.
     */
    public void deregister(ProductFacadeListener listener) {
	listeners.remove(listener);
    }

    /**
     * Return non-empty rack for product.
     * 
     * @param kind
     *            The kind of the product. Cannot be null.
     */
    public ProductRack getNonEmptyRackForProduct(ProductKind kind){
    	ProductRack res = null;
    	List<Integer> list = getRacksForProductKind(kind);
    	for(Integer index : list) {
    	    ProductRack rack = hf.getProductRack(index);
    	    int size = rack.size();
    	    if(size > 0) {
    	    	res = rack;
    	    }
    	}
    	
    	return res;
    }
    
    /**
     * Loads the sequence of products into this facade. Produces zero or more
     * &quot:productAdded&quot; events whose total count arguments equal the
     * number of added products.
     * 
     * @param kind
     *            The product kind of the products to be loaded. Must be valid
     *            for this machine.
     * @param products
     *            The sequence of products to be loaded. Can be empty. None of
     *            these can be null.
     * @throws CapacityExceededException
     *             If there is insufficient free space to load all the products.
     * @throws UnsupportedProductKindException
     *             If the kind is not supported by this machine.
     */
    public void load(ProductKind kind, Product... products) throws CapacityExceededException, UnsupportedProductKindException {
	if(kind == null || products == null){
		IllegalArgumentException ex = new IllegalArgumentException();
	    throw ex;
	}
	
	List<Integer> list = getRacksForProductKind(kind);
	int productIndex = 0;
	for(Integer index : list) {
	    ProductRack rack = hf.getProductRack(index);
	    boolean hasSpace = rack.hasSpace();
	    while(hasSpace) {
	    	Product product = products[productIndex];
	    	rack.load(product);
	    	productIndex++;
	    	if(productIndex >= products.length) {
	    		notifyProductsAdded(kind, products.length);
	    		return;
	    	}
	    	hasSpace = rack.hasSpace();
	    }
	}

	if(productIndex < products.length){
		CapacityExceededException ex = new CapacityExceededException();
	    throw ex;
	}
    }

    public List<Product> unload(){
    	List<Product> results = new ArrayList<>();
    	for(ProductKind kind:availableKinds){
    		List<Product> products = unload(kind);
    		results.addAll(products);
    	}
    	return results;
    }
    /**
     * Removes all the products of a specified kind from this controller.
     * Produces zero or more &quot;productRemoved&quot; events whose total count
     * arguments equal the number of removed products.
     * 
     * @param kind
     *            The kind of the products to unload. Cannot be null.
     * @return A list of the unloaded products. Cannot be null. Can be empty.
     * @throws UnsupportedProductKindException
     *             If the kind is not supported by this machine.
     */
    public List<Product> unload(ProductKind kind) throws UnsupportedProductKindException {
	List<Product> results = new ArrayList<>();
	List<Integer> indices = getRacksForProductKind(kind);

	if(indices == null){
		UnsupportedProductKindException ex = new UnsupportedProductKindException();
	    throw ex;
	}

	for(Integer index : indices) {
	    ProductRack rack = hf.getProductRack(index);
	    List<Product> list = rack.unload();
	    results.addAll(list);
	}

	int size = results.size();
	notifyProductsRemoved(kind, size);

	return results;
    }

    private void notifyProductsRemoved(ProductKind kind, int count) {
	for(ProductFacadeListener l : listeners)
	    l.productsRemoved(this, kind, count);
    }

    private void notifyProductsAdded(ProductKind kind, int count) {
	for(ProductFacadeListener l : listeners)
	    l.productsAdded(this, kind, count);
    }

    /**
     * Accesses the {@link ProductKind} for the {@link ProductRack} at the
     * indicated index.
     * 
     * @param index The index of the {@link ProductRack} of interest.
     * @return The corresponding kind, or null if it does not exist.
     */
    public ProductKind getProductKindForRack(int index) {
    	ProductKind kind = rackToKindMap.get(index);
    	return kind;
    }
    
    private List<Integer> getRacksForProductKind(ProductKind target){
    	List<Integer> list = new ArrayList<Integer>();
    	Set<Integer> keys = rackToKindMap.keySet();
    	for(int rack:keys){
    		ProductKind kind = rackToKindMap.get(rack);
    		boolean equal = kind.equals(target);
    		if(equal){
    			list.add(rack);
    		}
    	}
    	
    	return list;
    }
}
